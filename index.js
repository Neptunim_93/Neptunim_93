const fetch = require('node-fetch');
const fs = require('fs');


function fetchData(url) {
  return fetch(url, {
    headers: {
      'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN
    }
  })
    .then(response => {
      if (!response.ok) {
        throw new Error('La requête a échoué avec le code de statut ' + response.status);
      }
      return response.json();
    });
}

function fetch2Data(url1, url2) {
  const json1Promise = fetchData(url1);
  const json2Promise = fetchData(url2);
  return Promise.all([json1Promise, json2Promise]).then(([data1, data2]) => [...data1, ...data2]);
}

function langProgress (language, usage, ligneIndex, color) {
  const deltaTime = 150 * ligneIndex;
  return ` <g transform="translate(0, ${40 * ligneIndex})">
    <g class="stagger" style="animation-delay: ${450 + deltaTime}ms">
      <text data-testid="lang-name" x="2" y="15" class="lang-name">${language}</text>
      <text x="215" y="34" class="lang-name">${usage}%</text>
      <svg width="205" x="0" y="25">
        <rect rx="5" ry="5" x="0" y="0" width="205" height="8" fill="#ddd"/>
        <svg data-testid="lang-progress" width="${usage}%">
          <rect height="8" fill=${color} rx="5" ry="5" x="0" y="0" class="lang-progress" style="animation-delay: ${750 + deltaTime}ms;"/>
        </svg>
      </svg>
    </g>
  </g>`;
}

function render(usage,valuesList, color) {
  let body = '';
  Object.keys(usage).forEach(language => {
    const index = valuesList.findIndex(item => item === usage[language]);
    body += langProgress(language, usage[language].toFixed(2), index, color[language]);
  });
  width=300
  height=55 + (40 * valuesList.length) + 30
  let svg = `
  <svg
    width="${width}"
    height="${height}"
    viewBox="0 0 ${width} ${height}"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    aria-labelledby="descId"
  >
    <title id="titleId"/>
    <desc id="descId"/>
    <style>
      .header {
        font: 600 18px 'Segoe UI', Ubuntu, Sans-Serif;
        fill: #2f80ed;
        animation: fadeInAnimation 0.8s ease-in-out forwards;
      }
      @supports(-moz-appearance: auto) {
        /* Selector detects Firefox */
        .header { font-size: 15.5px; }
      }
      @keyframes slideInAnimation {
        from {
          width: 0;
        }
        to {
          width: calc(100%-100px);
        }
      }
      @keyframes growWidthAnimation {
        from {
          width: 0;
        }
        to {
          width: 100%;
        }
      }
      .stat {
        font: 600 14px 'Segoe UI', Ubuntu, "Helvetica Neue", Sans-Serif; fill: #434d58;
      }
      @supports(-moz-appearance: auto) {
        /* Selector detects Firefox */
        .stat { font-size:12px; }
      }
      .bold { font-weight: 700 }
      .lang-name {
        font: 400 11px "Segoe UI", Ubuntu, Sans-Serif;
        fill: #434d58;
      }
      .stagger {
        opacity: 0;
        animation: fadeInAnimation 0.3s ease-in-out forwards;
      }
      #rect-mask rect{
        animation: slideInAnimation 1s ease-in-out forwards;
      }
      .lang-progress{
        animation: growWidthAnimation 0.6s ease-in-out forwards;
      }
      /* Animations */
      @keyframes scaleInAnimation {
        from {
          transform: translate(-5px, 5px) scale(0);
        }
        to {
          transform: translate(-5px, 5px) scale(1);
        }
      }
      @keyframes fadeInAnimation {
        from {
          opacity: 0;
        }
        to {
          opacity: 1;
        }
      }
    </style>
    <rect data-testid="card-bg" x="0.5" y="0.5" rx="4.5" height="99%" stroke="#e4e2e2" width="299" fill="#fffefe" stroke-opacity="1"/>
    <g data-testid="card-title" transform="translate(25, 35)">
      <g transform="translate(0, 0)">
        <text x="0" y="0" class="header" data-testid="header">Most Used Languages</text>
      </g>
    </g>
    <g data-testid="main-card-body" transform="translate(0, 55)">
      <svg x="25">
        ${body}
      </svg>
    </g>
    <style xmlns="" data-source="base" class="dblt-ykjmwcnxmi"/><style xmlns="" data-source="stylesheet-processor" class="dblt-ykjmwcnxmi"/>
  </svg>`;
  return svg;
}

function getKeyOfValue(targetValue) {
  for (const key in data) {
    if (data[key] === targetValue) {
      foundKey = key;
      break;
    }
  }
  return foundKey;
}

function main() {
  const user = process.env.USER;
  const apiUrl = `https://gitlab.com/api/v4/users/${user}/projects?simple=true&per_page=10000000000`;
  const apiUrl1 = `https://gitlab.com/api/v4/users/${user}/contributed_projects?simple=true&per_page=10000000000`;
  const data3Dictionary = {};
  let totalSize = 0;
  const test = fetch2Data(apiUrl,apiUrl1)
    .then(data => {
      const promises = data.map(project => {
        const apiUrl2 = `https://gitlab.com/api/v4/projects/${project.id}?statistics=true`;
        return fetchData(apiUrl2)
          .then(data2 => {
            if (data2.statistics.repository_size !== 0) {
              const apiUrl3 = `https://gitlab.com/api/v4/projects/${project.id}/languages`;
              totalSize += data2.statistics.repository_size;
              return fetchData(apiUrl3)
                .then(data3 => {
                  data3Dictionary[project.name] = [data3,data2.statistics.repository_size];
                })
                .catch(error => {
                  console.error('Erreur de requête :', error);
                });
            }
          })
          .catch(error => {
            console.error('Erreur de requête :', error);
          });
      });
      return Promise.all(promises);
    })
    .then(() => {
      let usage = {}
      Object.keys(data3Dictionary).forEach(cle => {
        Object.keys(data3Dictionary[cle][0]).forEach(languages => {
          if (languages in usage)
          {
            usage[languages] = (data3Dictionary[cle][1] * data3Dictionary[cle][0][languages] )/totalSize + usage[languages];
          }
          else
          {
            usage[languages] = (data3Dictionary[cle][1] * data3Dictionary[cle][0][languages] )/totalSize;
          }
        });
      });
      const valuesList = Object.values(usage).sort(function (a,b) { return b - a });
      return fetch('https://raw.githubusercontent.com/github/linguist/master/lib/linguist/languages.yml')
        .then(response => {
          if (!response.ok) {
            throw new Error('La requête a échoué');
          }
          return response.text();
        })
        .then(yamlData => {
          dicColor = parseYAML(yamlData);
          const svg = render(usage,valuesList,dicColor);
          fs.writeFileSync('languages.svg', svg);
          return svg;
        })
        .catch(error => {
          console.error('Une erreur s\'est produite lors de la récupération du fichier YAML :', error);
        });
    })
    .catch(error => {
      console.error('Erreur de requête :', error);
    });
}

function getKey(targetValue, usage) {
  let foundKey = null;
  for (const key in usage) {
    if (usage[key] === targetValue) {
      foundKey = key;
      break;
    }
  }
  return foundKey;
}

function parseYAML(yamlString) {
  const yamlLines = yamlString.split('\n');
  const result = {};
  let currentLanguage = {};
  for (const line of yamlLines) {
    if (line.trim() === '' || line.trim().startsWith('#') || line.trim().startsWith('- ')) {
      continue;
    }
    if (line.includes(':')) {
      const [key, value] = line.split(':');
      const trimmedKey = key.trim();
      const trimmedValue = value.trim();
      if (trimmedKey === 'fs_name') {
      } else if (trimmedKey === 'color') {
        currentLanguage.color = trimmedValue;
      } else if (trimmedKey === 'aliases') {
      } else if (trimmedKey === 'language_id') {
      } else if (trimmedKey === 'group') {
      } else if (trimmedKey === 'type') {
      } else if (trimmedKey === 'extensions') {
      } else if (trimmedKey === 'ace_mode') {
      } else if (trimmedKey === 'tm_scope') {
      } else if (trimmedKey === 'codemirror_mode') {
      } else if (trimmedKey === 'codemirror_mime_type') {
      } else if (trimmedKey === 'filenames') {
      } else if (trimmedKey === 'reStructuredText') {
      } else if (trimmedKey === 'interpreters') {
      } else if (trimmedKey === 'wrap') {
      } else {
        currentLanguage.name = trimmedKey;
      }
    }
    if (currentLanguage && currentLanguage.name && currentLanguage.color) {
      result[currentLanguage.name] = currentLanguage.color;
    }
  }
  return result;
}

main();